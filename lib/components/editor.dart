import 'package:flutter/material.dart';

class Editor extends StatelessWidget {
  final TextEditingController controlador;
  final String rotulo;
  final String dica;
  final IconData icone;

  /**
   * Para que seja possível passar os parâmetros para a instância da classe na ordem
   * que quiser e para que não seja necessário passar todos os parâmetros, é necessário
   * envolver os parâmetros na declaração do construtor em {}, e na instância
   * da classe, passar os valores para cada atributo da classe, por exemplo:
   * Editor(controlador: _controladorCampoValor, dica: "0.00", rotulo: "Valor")
   * Veja que não foi informado o valor para o atributo "icone" e o valor da "dica"
   * foi informado antes do valor do "rotulo"
   */
  Editor({
    this.controlador,
    this.rotulo,
    this.dica,
    this.icone,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: TextField(
        controller: controlador,
        // através do atributo "controller" conseguimos recuperar o valor digitado para trabalharmos com ele
        style: TextStyle(
            fontSize:
                24.0 // O material design utiliza fontes na base 8, ou seja, o tamanho padrão é 16
            ),
        decoration: InputDecoration(
          icon: icone != null ? Icon(icone) : null,
          labelText: rotulo,
          hintText: dica,
        ),
        keyboardType: TextInputType.number,
      ),
    );
  }
}
