import 'package:bytebank/components/editor.dart';
import 'package:bytebank/models/transferencia.dart';
import 'package:flutter/material.dart';

const _tituloAppBar = 'Criando Transferência';

const _rotuloCampoValor = 'Valor';
const _dicaCampoValor = '0.00';

const _rotuloCampoNumeroConta = 'Número da conta';
const _dicaCampoNumeroConta = '0000';

const _textoBotaoConfirmar = 'Confirmar';

class FormularioTransferencia extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return FormularioTransferenciaState();
  }
}

class FormularioTransferenciaState extends State<FormularioTransferencia> {
  /**
   * Constantes que armazenam os valores dos inputs do formulario de transferência.
   * Observar que dentro da classe TextField instanciada na Column existe uma propriedade
   * chamada "controller" que usa essa constante para salvar o valor digitado pelo usuário
   */
  final TextEditingController _controladorCampoNumeroConta =
      TextEditingController();
  final TextEditingController _controladorCampoValor = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(_tituloAppBar),
      ),
      body: SingleChildScrollView(
        // Esse Widget envolve outro widget (no caso, o "Column" e dá o comportamento de scroll. Corrige o bug onde um dos campos do formulário nao aparecia quando o usuário usava o celular na horizontal com rotação de tela
        child: Column(
          // Para criar uma column no Android Studio, basta apertar Alt + Enter e selecionar a opção "Wrap with Column"
          children: <Widget>[
            Editor(
              controlador: _controladorCampoNumeroConta,
              rotulo: _rotuloCampoNumeroConta,
              dica: _dicaCampoNumeroConta,
            ),
            Editor(
              controlador: _controladorCampoValor,
              rotulo: _rotuloCampoValor,
              dica: _dicaCampoValor,
              icone: Icons.monetization_on,
            ),
            RaisedButton(
              child: Text(_textoBotaoConfirmar),
              onPressed: () {
                _criaTransferencia(context);
              },
            ),
          ],
        ),
      ),
    );
  }

  /**
   * Para extrair uma lógica e transforma-la em uma função, basta selecionar o trecho desejado
   * e apertar Ctrl + Alt + M. Essa abordagem foi utilizada na função abaixo, que tinha
   * sua lógica escrita dentro do evento "onPressed" da classe "FormularioTransferencia"
   * Função responsável por receber os valores digitados nos campos relativos ao numero da conta
   * e ao valor e criar uma instância de uma trasferência com eles
   */
  void _criaTransferencia(BuildContext context) {
    final int numeroConta = int.tryParse(_controladorCampoNumeroConta.text);
    final double valor = double.tryParse(_controladorCampoValor.text);

    if (numeroConta != null && valor != null) {
      final transferenciaCriada = Transferencia(valor, numeroConta);
      Navigator.pop(context,
          transferenciaCriada); // o primeiro parâmetro é padrão, o segundo é o que vai voltar para o future
    }
  }
}
