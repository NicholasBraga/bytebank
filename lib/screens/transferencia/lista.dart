import 'package:bytebank/models/transferencia.dart';
import 'package:bytebank/screens/transferencia/formulario.dart';
import 'package:flutter/material.dart';

const _tituloAppBar = 'Transferências';

/**
 * Para que uma classe possa ser usada como um widget que já existe (não um que será criado do zero)
 * ela tem que extender ou de "StatefulWidget" ou de "StatelessWidget".
 * A diferença é que:
 * StatelessWidget - não tem alteração de propriedades
 * StatefulWidget - tem alteração de propriedaddes
 *
 * Classe responsável por implementar o statefulwidget, conter a estrutura de dado
 * que representa a lista de transferencias e chamar a "ListaTransferenciasState", que
 *
 *
 */
class ListaTransferencias extends StatefulWidget {
  // Estrutura de dados que armazenará a lista de transferências a ser construída dinamicamente, a cada transferência feita pelo usuário
  final List<Transferencia> _transferencias = List();

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ListaTransferenciasState();
  }
}

/**
 * Classe responsável por trabalhar o state da lista de transferências,
 * recebendo o tipo "ListaTransferencias", além de implementar o botão
 * para a chamada do formulário de transferências e conter o listener (future)
 * para receber o valor digitado nos campos do formulário e exibí-lo em forma
 * de listagem.
 * Internamente, conseguiremos acessar os atributos da classe "ListaTransferencias" através da palavra "widget"
 */
class ListaTransferenciasState extends State<ListaTransferencias> {
  @override
  Widget build(BuildContext context) {
    // É o build que atualiza o conteúdo na tela
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(_tituloAppBar),
      ),
      body: ListView.builder(
        itemCount: widget._transferencias.length,
        //Como "_transferencias" é uma propriedade da classe "ListaTransferencias", podemos acessá-la graças ao "extends State<ListaTransferencias> através da palavra "widget"
        // determina a quantidade de transferencias
        /**
         * O primeiro parametro é um contexto e o segundo é o índice de cada elemento
         * a ser mostrado na tela
         */
        itemBuilder: (context, indice) {
          final transferencia = widget._transferencias[indice];
          return ItemTransferencia(transferencia);
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            // o future fica atento para caso venha alguma informação digitada pelo usuário através do navigator
            return FormularioTransferencia();
          })).then((transferenciaRecebida) {
            // quando o future receber alguma informação, ele libera a informação para ser trabalhada
            _atualiza(transferenciaRecebida);
          });
        },
      ),
    );
  }

  void _atualiza(Transferencia transferenciaRecebida) {
    if (transferenciaRecebida != null) {
      /**
       * O setState é a forma correta de atualizar um stateful widget. Em conjunto com o Future,
       * ele grava o novo state do widget quando a ação assincrona do future tiver sido concluída
       */
      setState(() {
        widget._transferencias.add(
            transferenciaRecebida); // Para cada transferência recebida, usamos o .add() para adicionar na lista chamada "_transferencias", que é lista usada para montar de forma dinâmica a lista de transferencias
      });
    }
  }
}

/**
 * Retorna a estrutura do card de cada transferência feita
 */
class ItemTransferencia extends StatelessWidget {
  // Instancia da classe "Transferencia"
  final Transferencia _transferencia;

  /**
   * Para criar o construtor automaticamente usando o Android Studio, basta apertar
   * Alt + Insert, selecionar as constantes e clicar em OK
   */
  ItemTransferencia(this._transferencia);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
      child: ListTile(
        leading: Icon(Icons.monetization_on),
        title: Text(_transferencia.valor.toString()),
        subtitle: Text(_transferencia.numeroConta.toString()),
      ),
    );
  }
}
