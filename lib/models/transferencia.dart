/**
 * Abstrai a lógica para lançamento dos dados da transferência, ou seja,
 * valor e número da conta
 */
class Transferencia {
  final double valor;
  final int numeroConta;

  Transferencia(
    this.valor,
    this.numeroConta,
  );

  @override
  String toString() {
    return 'Transferencia{valor: $valor, numeroConta: $numeroConta}';
  }
}
