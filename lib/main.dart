import 'package:bytebank/screens/transferencia/lista.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/**
 * Para que o app possa ser executado, uma instância de um widget precisa ser passada
 * para a função "runApp()"
 */

/**
 * Widgets são elementos/componentes visuais da tela de um app feito com Flutter.
 * Uma tela pode ter 1 ou N Widgets
 */

/**
 * No flutter, usar o CTRL + P para ver quais são os argumentos esperados por uma classe
 * e CTRL + Q para ver qual é o tipo de dado do argumento e a documentação da classe
 */

/**
 * Se selecionar um widget e apertar CTRL + D, o Android Studio já faz
 * uma cópia do widget automaticamente
 */

/**
 * Na lateral direita do Android Studio, é possível ver a árvore de widgets clicando
 * no botão "Flutter Outline"
 */

/**
 * Quando usamos o MaterialApp, podemos sempre considerar como "Home" uma instância
 * do Scaffold().
 * Pra montar um app tradicional, podemos usar a propriedade "appBar : AppBar()"
 */
void main() => runApp(BytebankApp());

class BytebankApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: ListaTransferencias(),
      theme: ThemeData(
        primaryColor: Colors.green[900], // a referência da cor, denotada entre colchetes, pode ser obtida no site material.io/resources/color
        accentColor: Colors.blueAccent[700],
        buttonTheme: ButtonThemeData(
          buttonColor: Colors.blueAccent[700],
          textTheme: ButtonTextTheme.primary,
        ),
      ),
    );
  }
}
